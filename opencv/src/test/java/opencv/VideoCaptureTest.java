package opencv;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;

import junit.framework.TestCase;

public class VideoCaptureTest extends TestCase{
	
	static {
		nu.pattern.OpenCV.loadShared();
		 System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        
	}
	
	public void testMatDump() {
		 Mat mat = Mat.eye(3, 3, CvType.CV_8UC1);
         System.out.println(mat.dump());
	}
	
	public void testCapture() {
		VideoCapture capture = new VideoCapture();
		boolean openResult = capture.open(0);
		assertTrue(openResult);
	}

}
